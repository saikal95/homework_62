import {Component, Input, OnInit} from '@angular/core';
import {Game} from "../shared/game.model";
import {GameService} from "../shared/game.service";

@Component({
  selector: 'app-one-game',
  templateUrl: './one-game.component.html',
  styleUrls: ['./one-game.component.css']
})
export class OneGameComponent {

  @Input() game!: Game;

  constructor(private gameService: GameService) {}

}
