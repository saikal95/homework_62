import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {GameService} from "../../shared/game.service";
import {Game} from "../../shared/game.model";

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.css']
})
export class NewGameComponent implements OnInit{

  games!: Game[];

  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('descriptionInput') descriptionInput!: ElementRef;
  @ViewChild('imageUrlInput') imageUrlInput!: ElementRef;
  @ViewChild('platformInput') platformInput!: ElementRef;

  constructor(private gameService: GameService) {}

  ngOnInit(): void {
    this.games = this.gameService.getGames();
    this.gameService.gameChange.subscribe((games: Game[]) => {
      this.games = games;
    });
  }

  createGame() {
    const name: string = this.nameInput.nativeElement.value;
    const description: string = this.descriptionInput.nativeElement.value;
    const imageUrl: string = this.imageUrlInput.nativeElement.value;
    const platform:string = this.platformInput.nativeElement.value;

    const game = new Game(name, imageUrl,platform, description);
    this.gameService.addGame(game);
    console.log(this.games);
  }



}
