import { Component, OnInit } from '@angular/core';
import {Game} from "../shared/game.model";
import {GameService} from "../shared/game.service";
import {ActivatedRoute, Params} from "@angular/router";

@Component({
  selector: 'app-manage-games',
  templateUrl: './manage-games.component.html',
  styleUrls: ['./manage-games.component.css']
})
export class ManageGamesComponent implements OnInit {
  games: Game [] = []
  game!: Game;
  gamesPlatform: string [] = [];


  constructor(private gameService:GameService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.games = this.gameService.getGames();
    this.gameService.gameChange.subscribe((games:Game[])=> {
      this.games = games;
    });

    this.games.forEach((item) => {
      if (this.gamesPlatform.includes(item.platform)) {
        return;
      } else {
        this.gamesPlatform.push(item.platform);
      }
      return this.gamesPlatform;
    })

  }

}
