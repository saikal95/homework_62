import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {Game} from "../../shared/game.model";
import {GameService} from "../../shared/game.service";

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.css']
})
export class GameDetailsComponent implements OnInit {

  game!: Game;
  newArray: any[] = [];

  constructor(private route: ActivatedRoute,
              private gameService: GameService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params)=>{
      const gameId = params['platformName'];
      this.game = this.gameService.getGame(gameId);
      this.newArray = this.gameService.getGamesbyPlatform(gameId);
      console.log(this.newArray);
    })
  }

}
