import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GamesComponent } from './games/games.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ManageGamesComponent } from './manage-games/manage-games.component';
import { GameDetailsComponent } from './manage-games/game-details/game-details.component';
import { NewGameComponent } from './manage-games/new-game/new-game.component';
import {GameService} from "./shared/game.service";
import {notFoundComponent} from "./not-found.component";
import {emptyGameComponent} from "./manage-games/empty-game.component";
import {FormsModule} from "@angular/forms";
import { HomeComponentComponent } from './home-component/home-component.component';
import { OneGameComponent } from './one-game/one-game.component';
import { SelectedGameComponent } from './manage-games/selected-game/selected-game.component';

@NgModule({
  declarations: [
    AppComponent,
    GamesComponent,
    ToolbarComponent,
    ManageGamesComponent,
    GameDetailsComponent,
    NewGameComponent,
    notFoundComponent,
    emptyGameComponent,
    HomeComponentComponent,
    OneGameComponent,
    SelectedGameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [GameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
