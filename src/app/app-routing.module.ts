import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ManageGamesComponent} from "./manage-games/manage-games.component";
import {NewGameComponent} from "./manage-games/new-game/new-game.component";
import {GameDetailsComponent} from "./manage-games/game-details/game-details.component";
import {notFoundComponent} from "./not-found.component";
import {emptyGameComponent} from "./manage-games/empty-game.component";
import {HomeComponentComponent} from "./home-component/home-component.component";
import {SelectedGameComponent} from "./manage-games/selected-game/selected-game.component";

const routes: Routes = [
  {path: '', component: HomeComponentComponent},
  {path: 'games', component: ManageGamesComponent, children: [
      {path: '', component: emptyGameComponent},
      {path: 'new', component: NewGameComponent},
      {path: ':platformName', component:GameDetailsComponent}
    ]},
  {path: '**', component: notFoundComponent},
  {path: 'selected',component: SelectedGameComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
